#include "common.h"

//process contours and find outer boundary and room boundary
void processPlanContours(Mat img, float &biggestContourArea, size_t &biggestContourIdx, vector<vector<Point>> &contours, vector<size_t> &room_ind);

//text detetection
vector<Rect> detectLetters(Mat img);

//OCR
Mat processForOCR(Mat img);
string recognizeText(Mat img);