#include "HeaderBeachAndOcean.h"

Mat mainBeachAndOcean(Mat img)
{
	//characteristics
	float totalArea = 0;
	size_t outer_boundary_ind = -1;
	vector<vector<Point>> all_contours;
	vector<size_t> room_ind;

	//create Mat to show output
	Mat out_img(img.size(), CV_8UC3, Scalar(255, 255, 255));

	//detect boundaries
	processPlanContours(img, totalArea, outer_boundary_ind, all_contours, room_ind);

	//draw outer boundary and rooms boundary on output Mat
	drawContours(out_img, all_contours, outer_boundary_ind, Scalar(255, 0, 0), 3);

	cout << "Area of outer boundary: " << totalArea << endl;

	cout << "No. of rooms: " << room_ind.size() << endl;

	//Detect Text
    vector<Rect> letterBBoxes = detectLetters(img);


	//process for each letter Box
	for(int i=0; i < letterBBoxes.size(); i++)
	{
		int tlx = letterBBoxes[i].x;
		int tly = letterBBoxes[i].y;
		int w = letterBBoxes[i].width;
		int h = letterBBoxes[i].height;

		Rect letterBox = Rect(tlx - 0.2*w, tly - 0.2*h, 1.4*w, 1.4*h);        
		
		Mat box;
		img(letterBox).copyTo(box);

		Mat input2ocr = processForOCR(box);

		string out_text = recognizeText(input2ocr);

		if(out_text != "")
		{
			string tess_output = out_text.substr(0, out_text.size()-2);
			cout << "text: " << tess_output << endl;
			Point2f rect_center = Point2f((letterBBoxes[i].x + letterBBoxes[i].width/2),(letterBBoxes[i].y+letterBBoxes[i].height/2));

			for(size_t j = 0; j < room_ind.size(); j++)
			{
				if(pointPolygonTest(all_contours[room_ind[j]], rect_center, false) >= 0 && 
					(tess_output.find("'") == string::npos || tess_output.find("""") == string::npos))
				{
					cout << "Area of room: " << tess_output << " is: " << contourArea(all_contours[room_ind[j]])*100/totalArea << 
							" % of total area approx. " << endl;

					drawContours(out_img, all_contours, room_ind[j], Scalar(0, 255, 0), 2);
				}
				putText(out_img, tess_output, Point(letterBBoxes[i].x, letterBBoxes[i].y ), FONT_HERSHEY_PLAIN, 1,  Scalar(0,0,255));

				//rectangle(out_img, letterBBoxes[i], Scalar(0,255,0),3,8,0);
			}
			
		}
	
	}

	return out_img;


}