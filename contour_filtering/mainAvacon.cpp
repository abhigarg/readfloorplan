#include "HeaderAvacon.h"


Mat mainAvacon(Mat img)
{
	//characteristics
	float totalArea = 0;
	size_t outer_boundary_ind = -1;
	vector<vector<Point>> all_contours;
	vector<size_t> large_ind, medium_ind, small_ind;

	//create Mat to show output
	Mat out_img(img.size(), CV_8UC3, Scalar(255, 255, 255));

	//detect boundaries
	processHierarchicalContours(img, totalArea, outer_boundary_ind, all_contours, large_ind, medium_ind, small_ind);

	//draw outer boundary and rooms boundary on output Mat
	drawContours(out_img, all_contours, outer_boundary_ind, Scalar(255, 0, 0), 3);

	cout << "Area of outer boundary: " << totalArea << endl;

	cout << "No. of rooms: " << large_ind.size() + medium_ind.size() << endl;

	//draw all remaining contours



	return out_img;

}