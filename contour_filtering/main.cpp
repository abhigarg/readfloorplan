#include "Header.h"



int main(int argc,char* argv[])
{
    //Read
	
	string input_img_path = argv[1];
	string input_img_fname = input_img_path.substr(0, input_img_path.find_last_of("."));

	cout << "input image name: " << input_img_fname << endl;

	string output_img_path = input_img_fname.append("_output.jpg");


	if(argc < 3)
	{
		cout << "usuage: test_project.exe <path of input image> <beach or avacon>" << endl;
		return -1;
	}

    Mat img = imread(argv[1]);
	Mat out_img;

	string process_type = argv[2];

	if(process_type == "beach")
		//apply processing for Beach and Ocean floor-plan
		out_img = mainBeachAndOcean(img);

	else //apply processing for Avacon floor plan
		out_img = mainAvacon(img);

	imshow("Output", out_img);

	char k = waitKey(0);
    
	if(k == 's')
		imwrite( output_img_path, out_img);

	return 1;

}