#include "common.h"

//process contours and find and room bondaries from the hiearachical contours
void processHierarchicalContours(Mat img, float &biggestContourArea, size_t &biggestContourIdx, vector<vector<Point>> &contours, 
								vector<size_t> &large_ind, vector<size_t> &medium_ind, vector<size_t> &small_ind);