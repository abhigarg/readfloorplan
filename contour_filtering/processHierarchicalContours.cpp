#include "common.h"

void processHierarchicalContours(Mat img, float &biggestContourArea, size_t &biggestContourIdx, vector<vector<Point>> &contours, 
								vector<size_t> &large_ind, vector<size_t> &medium_ind, vector<size_t> &small_ind)
{

	Mat gray;

	if(img.channels() == 3)
		cvtColor(img, gray, CV_BGR2GRAY);
	else
		img.copyTo(gray);

	Mat thresh;
	threshold(gray, thresh, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

	imshow("threshold", thresh);
	//waitKey(0);

	//vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;
	findContours(thresh, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);  //CV_RETR_CCOMP  CV_RETR_TREE

	//find biggest contour or outer most boundary
	//biggestContourIdx = -1;
	

	Mat drawImg(thresh.size(), CV_8UC3, Scalar(255, 255, 255));

	Scalar color = Scalar(0, 255, 0);

	Scalar lowest_h_color = Scalar(255, 255, 0);

	for(size_t i = 0; i < contours.size(); i++)
	{
		float ctArea = contourArea(contours[i]);
		
		if(ctArea < 0.9*thresh.rows*thresh.cols)
		{
			//drawContours(drawImg, contours, i, color, 1, 8, hierarchy, 0, Point());
		
			if(ctArea > biggestContourArea)
			{
				biggestContourArea = ctArea;
				biggestContourIdx = i;
			}
		}
	}
	//if no contour found
	if(biggestContourIdx < 0)
	{
		cout << "no contour found " << endl;

		biggestContourArea = -1;		

	}			
	else
	{	
		//draw contours to a %age of biggest area
		for(size_t i = 0; i < contours.size(); i++)
		{
			float ctArea = contourArea(contours[i]);

			if(hierarchy[i][3] != -1)  //for avalonbay
			{
				if(ctArea < 0.6*biggestContourArea && ctArea > 0.2*biggestContourArea)  //0.6 and 0.2 for avalonbay
				{
					drawContours(drawImg, contours, i, color, 1, 8, hierarchy, 0, Point());
					large_ind.push_back(i);
				}
				if(ctArea < 0.2*biggestContourArea && ctArea > 0.05*biggestContourArea) //0.2 and 0.05 for avalonbay
				{
					drawContours(drawImg, contours, i, Scalar(0, 0, 255), 1, 8, hierarchy, 0, Point());
					medium_ind.push_back(i);
				}

				if(ctArea < 0.005*biggestContourArea && ctArea > 0.0002*biggestContourArea) //0.05 and 0.0002 for avalonbay
				{
					drawContours(drawImg, contours, i, Scalar(255, 0, 0), 1, 8, hierarchy, 0, Point());
					small_ind.push_back(i);
				}
			}
		
		
		//cout << "Area of largest boundary: " << biggestContourArea << endl;
		}
		
		//draw largest boundary
		drawContours(drawImg, contours, biggestContourIdx, Scalar(255, 0, 0), 2, 8, hierarchy, 0, Point());

		imshow("Boundaries", drawImg);

		char k = waitKey(0);

		if(k == 's')
			imwrite("boundaries.jpg", drawImg);
			
		//}
		
	}


}