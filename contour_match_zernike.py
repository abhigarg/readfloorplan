# -*- coding: utf-8 -*-
"""
Created on Sat Dec 26 19:24:53 2015

@author: ABGG
"""
import sys
import numpy as np
import cv2
from pyimagesearch.zernikemoments import ZernikeMoments
from scipy.spatial import distance as dist

#inputs
query_image_path = sys.argv[1]

print "input query image: " 
print query_image_path


template_image_path = sys.argv[2]
print "template image: " 
print template_image_path


desc = ZernikeMoments(21)
    
#process template image, convert to grayscale and find contours for it
template_image = cv2.imread(template_image_path)

if template_image is not None:

    ht, wd, ch = template_image.shape[:3]
    
    if ch == 3:        
        gray_template = cv2.cvtColor(template_image, cv2.COLOR_BGR2GRAY) 
    
    if ch == 1:
        gray_template = np.empty_like (template_image)
        gray_template[:] = template_image
    
    ret_temp,thresh_template = cv2.threshold(gray_template,128,255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    
    cv2.imshow("threshold template", thresh_template)        
    
    (_,cnts_template, _) = cv2.findContours(thresh_template.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
    #cnts_template = sorted(cnts_template, key = cv2.contourArea, reverse = True)[0]
    outline = np.zeros(gray_template.shape, dtype = "uint8")

    cv2.drawContours(outline, cnts_template, -1, 255, 1)   

    moments_template = desc.describe(outline)
     
    cv2.imshow("template contours", outline)
    cv2.waitKey(0)
    
    # load the query image, convert it to grayscale, and
    # find contours for it
    image = cv2.imread(query_image_path)

    if image is not None:
    
        h,w,c = image.shape[:3]
        
        if c == 3:    
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if c == 1:
            gray = np.empty_like (image)
            gray[:] = image
        
        ret,thresh = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY)
        
        cv2.imshow("threshold image", thresh)    
        
        (_,cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cnts = sorted(cnts, key = cv2.contourArea, reverse = True)
        largestArea = cv2.contourArea(cnts[1])
        print "largest area:"
        print largestArea        
        
        outline_image = np.zeros(gray.shape, dtype = "uint8") 
        cv2.drawContours(outline_image, cnts, -1, 255, 1)
        cv2.imshow("image contours", outline_image)
        
        black = np.zeros(gray.shape, dtype = "uint8")        
        
        min_dist = 0
        result = -1      
        ind = -1
        for c in cnts:
            ind = ind + 1            
            #output = np.zeros(gray.shape, dtype = "uint8")
            #cv2.drawContours(output, c, -1, 255, 1)
            if cv2.contourArea(c) > 0.005*largestArea and cv2.contourArea(c) < 0.5*largestArea:
                x,y,w,h = cv2.boundingRect(c)
                
                output = cv2.resize(thresh[y:y+h,x:x+w], (wd,ht), 0, 0) 
                
                ret,thresh_out = cv2.threshold(output,128,255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
                
                (_,cnts_out, _) = cv2.findContours(thresh_out.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)                
                
                image_outline = np.zeros(output.shape, np.uint8)
                                
                cv2.drawContours(image_outline, cnts_out, -1, 255, 1)
                moments = desc.describe(image_outline)
                d = dist.euclidean(moments, moments_template)                
                
                if(min_dist == 0):
                    min_dist = d
                
                if(min_dist > d):
                    min_dist = d
                    result = ind
                
        print "output: "
        print "min distance: "
        print min_dist 
        cv2.drawContours(image, cnts, result, (255, 0, 0), 1)
        cv2.imshow("output", image)
        cv2.waitKey(0)
            
#                cv2.imshow("partial contours", image_outline)                
#                if cv2.waitKey(0) == ord('q'):
#                    break
                #output.fill(0)
    else:
        print "image not found"
    
else:
    print "template image not found"



cv2.destroyAllWindows()

print "done!!"