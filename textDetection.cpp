#include "Header.h"

vector<Rect> detectLetters(Mat img)
{
    vector<Rect> boundRect;
    
	Mat img_gray, img_sobel, img_threshold, element;
    cvtColor(img, img_gray, CV_BGR2GRAY);
    
	Sobel(img_gray, img_sobel, CV_8U, 1, 0, 3, 1, 0, BORDER_DEFAULT);
    threshold(img_sobel, img_threshold, 0, 255, CV_THRESH_OTSU+CV_THRESH_BINARY);
    element = getStructuringElement(MORPH_RECT, Size(17, 3) );
    morphologyEx(img_threshold, img_threshold, CV_MOP_CLOSE, element); //Does the trick
    
	//imshow("threshold", img_threshold);

	Mat mask(img.size(), CV_8UC1, Scalar(0));

	vector< vector< Point> > contours;
    findContours(img_threshold, contours, 0, 1); 
    vector<vector<cv::Point> > contours_poly( contours.size() );
    for( int i = 0; i < contours.size(); i++ )
	{
        if (contours[i].size()>100)
        { 
            approxPolyDP( Mat(contours[i]), contours_poly[i], 3, true );
            Rect appRect( boundingRect( Mat(contours_poly[i]) ));

			int tlx = appRect.x;
			int tly = appRect.y;
			int w = appRect.width;
			int h = appRect.height;

            if (appRect.width>appRect.height) 
				boundRect.push_back(appRect);
				//rectangle(mask, appRect, Scalar(255), -1);
				//rectangle(mask, Rect(tlx-0.2*w, tly-0.2*h, 1.4*w, 1.4*h), Scalar(255), -1);
        }
	}

	/*
	imshow("mask", mask);
	waitKey(0);

	vector<vector<Point>> cnts;
	findContours(mask, cnts, 0, 1); 
    vector<vector<Point> > cnts_poly( cnts.size() );
    for( int i = 0; i < cnts.size(); i++ )
		boundRect.push_back(boundingRect(Mat(cnts[i])));
		*/
    return boundRect;
}



void roomDetect(Mat img, float &biggestContourArea, size_t &biggestContourIdx, vector<vector<Point>> &contours, vector<size_t> &room_ind)
{

	Mat gray;

	if(img.channels() == 3)
		cvtColor(img, gray, CV_BGR2GRAY);
	else
		img.copyTo(gray);

	Mat thresh;
	threshold(gray, thresh, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

	imshow("threshold", thresh);
	//waitKey(0);

	//vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;
	findContours(thresh, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);  //CV_RETR_CCOMP  CV_RETR_TREE

	//find biggest contour or outer most boundary
	//biggestContourIdx = -1;
	

	Mat drawImg(thresh.size(), CV_8UC3, Scalar(255, 255, 255));

	Scalar color = Scalar(0, 255, 0);

	Scalar lowest_h_color = Scalar(255, 255, 0);

	for(size_t i = 0; i < contours.size(); i++)
	{
		float ctArea = contourArea(contours[i]);
		
		if(ctArea < 0.9*thresh.rows*thresh.cols)
		{
			//drawContours(drawImg, contours, i, color, 1, 8, hierarchy, 0, Point());
		
			if(ctArea > biggestContourArea)
			{
				biggestContourArea = ctArea;
				biggestContourIdx = i;
			}
		}
	}
	//if no contour found
	if(biggestContourIdx < 0)
	{
		cout << "no contour found " << endl;

		biggestContourArea = -1;		

	}			
	else
	{	
		//draw contours to a %age of biggest area
		for(size_t i = 0; i < contours.size(); i++)
		{
			float ctArea = contourArea(contours[i]);

			//if(hierarchy[i][3] != -1)  //for avalonbay
			//{
				if(ctArea < 0.95*biggestContourArea && ctArea > 0.7*biggestContourArea)  //0.6 and 0.2 for avalonbay
					drawContours(drawImg, contours, i, color, 1, 8, hierarchy, 0, Point());

				if(ctArea < 0.2*biggestContourArea && ctArea > 0.005*biggestContourArea) //0.2 and 0.05 for avalonbay
				{
					drawContours(drawImg, contours, i, Scalar(0, 0, 255), 1, 8, hierarchy, 0, Point());
					room_ind.push_back(i);
				}

				//if(ctArea < 0.005*biggestContourArea && ctArea > 0.0002*biggestContourArea) //0.05 and 0.0002 for avalonbay
					//drawContours(drawImg, contours, i, Scalar(255, 0, 0), 1, 8, hierarchy, 0, Point());
			//}
		
		
		//cout << "Area of largest boundary: " << biggestContourArea << endl;
		}
		
		//draw largest boundary
		drawContours(drawImg, contours, biggestContourIdx, Scalar(255, 0, 0), 2, 8, hierarchy, 0, Point());

		imshow("Boundaries", drawImg);

		char k = waitKey(0);

		if(k == 's')
			imwrite("boundaries.jpg", drawImg);
			
		//}
		
	}


}


string recognizeText(Mat img)
{
	tesseract::TessBaseAPI tess;
	tess.Init("", "eng", tesseract::OEM_DEFAULT);	
	tess.SetPageSegMode(tesseract::PSM_AUTO);	
	tess.SetImage((uchar*)img.data, img.cols, img.rows, 1, img.step1());
	tess.SetVariable("debug_file", "tesseract.log");

	char* txt_out;

	txt_out = tess.GetUTF8Text();

	//cout << "Text read: " << txt_out << "\n";
	
	//cout << "confidence score: " << tess.MeanTextConf() << endl;
	
	if(tess.MeanTextConf() > 85)
	{
		string out(txt_out);
		return out;
	}
	else
		return "";

	//return out;

}

Mat processForOCR(Mat img)
{
	Mat	gray, grad; 
	cvtColor(img, gray, CV_BGR2GRAY);
	
	Mat up_gray_img;
	resize(gray, up_gray_img, Size(0,0), 2.677, 2.677,CV_INTER_LINEAR);
					
	Mat smooth_img;
	GaussianBlur(up_gray_img, smooth_img, Size(), 6.8);
	
	Mat lowContMask = abs(up_gray_img - smooth_img) < 1;
	Mat unsharp_img = up_gray_img*(3.69) + smooth_img*(-2.69);

	up_gray_img.copyTo(unsharp_img, lowContMask);	

	return unsharp_img;
}


int main(int argc,char* argv[])
{
    //Read
	
	string input_img_path = argv[1];
	string input_img_fname = input_img_path.substr(0, input_img_path.find_last_of("."));

	cout << "input image name: " << input_img_fname << endl;

	string output_img_path = input_img_fname.append("_output.jpg");


	if(argc < 2)
	{
		cout << "usuage: test_project.exe <path of input image>" << endl;
		return -1;
	}

    Mat img = imread(argv[1]);  

	Mat gray;
	cvtColor(img, gray, CV_BGR2GRAY);

	//characteristics
	float totalArea = 0;
	size_t outer_boundary_ind = -1;;
	vector<vector<Point>> all_contours;
	vector<size_t> room_ind;

	//create Mat to show output
	Mat out_img(img.size(), CV_8UC3, Scalar(255, 255, 255));

	//detect boundaries
	roomDetect(img, totalArea, outer_boundary_ind, all_contours, room_ind);

	cout << "Area of outer boundary: " << totalArea << endl;

	cout << "No. of rooms: " << room_ind.size() << endl;

	//draw outer boundary and rooms boundary on output Mat
	drawContours(out_img, all_contours, outer_boundary_ind, Scalar(255, 0, 0), 3);

	//rooms
	//for(size_t i = 0; i < room_ind.size(); i++)
		//drawContours(out_img, all_contours, room_ind[i], Scalar(0, 255, 0), 2);

	//Detect Text
    vector<Rect> letterBBoxes = detectLetters(img);
    
	cout << "letter boxes size: " << letterBBoxes.size() << endl;
	
    //Display
	string bedroom_str = "BEDROOM";
	string kitchen_str = "KITCHEN";
	string livingroom_str = "LIVING";
	string balcony_str = "BALCONY";
	string dining_str = "DINING";
	string closet_str = "CLOSET";

    for(int i=0; i < letterBBoxes.size(); i++)
	{
		int tlx = letterBBoxes[i].x;
		int tly = letterBBoxes[i].y;
		int w = letterBBoxes[i].width;
		int h = letterBBoxes[i].height;

		Rect letterBox = Rect(tlx - 0.2*w, tly - 0.2*h, 1.4*w, 1.4*h);        
		
		Mat box;
		img(letterBox).copyTo(box);

		Mat input2ocr = processForOCR(box);

		//cvtColor(box, input2ocr, CV_BGR2GRAY);

		//imshow("text OCR", input2ocr);
		//waitKey(0);

		//destroyWindow("text OCR");

		string out_text = recognizeText(input2ocr);		
		
		/*if(out_text.find("BEDROOM") != std::string::npos || 
			out_text.find("LIVING ROOM") != std::string::npos || 
			out_text.find("CLOSET") != std::string::npos || 
			out_text.find("KITCHEN") != std::string::npos ||
			out_text.find("DINING") != std::string::npos ||
			out_text.find("BALCONY") != std::string::npos)*/
		if(out_text != "")
		{
			string tess_output = out_text.substr(0, out_text.size()-2);
			cout << "text: " << tess_output << endl;
			Point2f rect_center = Point2f((letterBBoxes[i].x + letterBBoxes[i].width/2),(letterBBoxes[i].y+letterBBoxes[i].height/2));

			for(size_t j = 0; j < room_ind.size(); j++)
			{
				if(pointPolygonTest(all_contours[room_ind[j]], rect_center, false) >= 0 && 
					(tess_output.find("'") == string::npos || tess_output.find("""") == string::npos))
				{
					cout << "Area of room: " << tess_output << " is: " << contourArea(all_contours[room_ind[j]])*100/totalArea << 
							" % of total area approx. " << endl;

					drawContours(out_img, all_contours, room_ind[j], Scalar(0, 255, 0), 2);
				}
				putText(out_img, tess_output, Point(letterBBoxes[i].x, letterBBoxes[i].y ), FONT_HERSHEY_PLAIN, 1,  Scalar(0,0,255));

				//rectangle(out_img, letterBBoxes[i], Scalar(0,255,0),3,8,0);
			}
			
		}
	
	}
	  		
	imshow("Output", out_img);

	char k = waitKey(0);
    
	if(k == 's')
		imwrite( output_img_path, out_img);

	//cout << "No. of rectangles found: " << letterBBoxes.size() << endl;
	
    return 0;
}